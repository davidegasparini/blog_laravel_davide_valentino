<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/home', function () {
    return view('home');
});

Route::get('/hack',function () {
    $name = "Davide";
    $students = [
         ["name" => "Davide",
         "surname" => "Gasparini"
         ],
         ["name" => "Giuseppe",
         "surname" => "Barecca"
         ],
         ["name" => "Alessandro",
         "surname" => "Firacano"
        ],
         ["name" => "veronica",
         "surname" => "torresin"
         ]

  
    ];
    return view('hack',compact("name","students"));
});

